package lets_buy_a_carr_app;

import lets_buy_a_carr_app.parser.Parser;

import java.text.ParseException;

public class BuyCarApp {
    public static void main(String[] args) throws ParseException {

        String personDataFromUser = "John Smith born 3/24/1984";

        Parser parser = new Parser();

        parser.setPersonLetterSequence(personDataFromUser);
        System.out.println(parser.getName());
        System.out.println(parser.getBirthDate());


        //TODO: Validate the date! in parser class
    }
}
