package lets_buy_a_carr_app.parser;

import lets_buy_a_carr_app.person.Person;

import javax.swing.text.DateFormatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

public class Parser {
    private String[] personLetterSequence;
    private String[] nameAndSurName = new String[2];
    //TODO: create date variable, or maybe String resembling date is fine.

    public Parser() {
    }

    public String[] getPersonLetterSequence() {
        return personLetterSequence;
    }

    public void setPersonLetterSequence(String personDataFromUser) {
        this.personLetterSequence = personDataFromUser.split(" ");
    }

    public String getName() {
        for (int i = 0; i < this.nameAndSurName.length; i++) {
            if (Character.isUpperCase(personLetterSequence[i].charAt(0))) {
                this.nameAndSurName[i] = this.personLetterSequence[i];
            } else {
                return "please enter name, surname correctly";
            }
        }
        return Arrays.toString(this.nameAndSurName);
    }

    public String getBirthDate() {
        String dateString = this.personLetterSequence[getPersonLetterSequence().length - 1];
        try {
            DateFormatter date = new DateFormatter();

            return dateString;
        } catch (Exception e) {
            System.out.println("Please enter valid date (mm/dd/yyyy): ");
            return null;
        }
    }

//
//    public Person createPerson(){
//
//    }


    @Override
    public String toString() {
        return "Parser{" +
                "personLetterSequence=" + Arrays.toString(personLetterSequence) +
                '}';
    }
}
