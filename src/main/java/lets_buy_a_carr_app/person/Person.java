package lets_buy_a_carr_app.person;

import java.util.Date;

public class Person {
    private String name;
    private String surName;
    private Date birtDate;

    public Person(String name, String surName, Date birtDate) {
        this.name = name;
        this.surName = surName;
        this.birtDate = birtDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Date getBirtDate() {
        return birtDate;
    }

    public void setBirtDate(Date birtDate) {
        this.birtDate = birtDate;
    }

    @Override
    public String toString() {
        return "Parser{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", birtDate=" + birtDate +
                '}';
    }
}
