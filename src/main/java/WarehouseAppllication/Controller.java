package WarehouseAppllication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Controller {
    private static Controller instance;

    public Controller() {
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }


    private WhareHouse whareHouse = WhareHouse.getInstance();


    public void addNewItem(String name, Item item) {
        whareHouse.getItemsList().put(name, item);
    }

    public void addToAnExistingItem(String name, Integer count) {
        Item item = getItemFromWarehouse(name);
        count += item.getCount();
        item.setCount(count);
        whareHouse.getItemsList().replace(name, item);
    }

    public void removeItem(String name, Integer count) {
        Item item = getItemFromWarehouse(name);
        count = item.getCount() - count;
        if (count > 0) {
            item.setCount(count);
        } else {
            item.setCount(0);
        }
        whareHouse.getItemsList().replace(item.getName(), item);
    }


    public void printAllItems() {
        System.out.println(Arrays.toString(whareHouse.getItemsList().values().toArray()));
    }

    //TODO: print
    public void printPriceOfAllItems() {
        List<Item> itemList = new ArrayList<>();

        Item[] items = whareHouse.getItemsList().values().toArray(new Item[0]);

        for (Item item :
                items) {

            System.out.println("price for all " + item.getName() + "s is " + item.getCount() * item.getPrice());
        }
    }

    public Item getItemFromWarehouse(String name) {
        return whareHouse.getItemsList().get(name);
    }
}
