package WarehouseAppllication;

import java.util.HashMap;
import java.util.Map;

public class WhareHouse {
    private static WhareHouse instance;

    private Map<String, Item> itemsList = new HashMap<>();


    public WhareHouse() {
    }

    public static WhareHouse getInstance() {
        if (instance == null) {
            instance = new WhareHouse();
        }
        return instance;
    }

    public Map<String, Item> getItemsList() {
        return itemsList;
    }
}
