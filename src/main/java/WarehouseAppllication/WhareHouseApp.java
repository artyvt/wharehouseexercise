package WarehouseAppllication;

import java.util.Scanner;

public class WhareHouseApp {
    private static boolean isRunning = true;
    private static Controller controller = Controller.getInstance();

    public static void main(String[] args) {

        System.out.println("\t\t\t\t\t\tWLECOME to Wharehouse application");
        System.out.println("Here you can manage it by adding items, removing, and displaying the status\n");

        while (isRunning) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose an operation\n\ttype: add_new, add, remove, print_status, price_for_all: ");
            userChoice(scanner);
        }

    }

    private static void userChoice(Scanner scanner) {
        String choice = scanner.nextLine().toUpperCase();

        try {
            switch (UserOptions.valueOf(choice)) {
                case ADD:
                    System.out.println("Add an item: ");
                    addItem(scanner);
                    System.out.println("Item count increased");
                    break;
                case ADD_NEW:
                    System.out.println("Add a new item to the wharehouse: ");
                    addNewItem(scanner);
                    System.out.println("Itame added");
                    break;
                case REMOVE:
                    System.out.println("Remove an item from wharehouse: ");
                    removeItem(scanner);
                    System.out.println("Item removed");
                    break;
                case PRINT_STATUS:
                    System.out.println("Status of wharehouse: ");
                    controller.printAllItems();
                    break;
                case PRICE_FOR_ALL:
                    System.out.println("Price for all objects in wharehouse: ");
                    controller.printPriceOfAllItems();
                    break;
                case EXIT:
                    System.out.println("Exiting application!");
                    isRunning =false;
                    break;
                default:

                    break;
            }
        } catch (Exception exception) {
            System.out.println("check your spelling! Operation choice typo!");
            System.out.println();
        }
    }

    private static void removeItem(Scanner scanner) {
        String name;
        Integer count;
        System.out.println("name: ");
        name = scanner.nextLine();
        System.out.println("count: ");
        count = Integer.valueOf(scanner.nextInt());
        System.out.println("price: ");
        controller.removeItem(name, count);
    }

    private static void addItem(Scanner scanner) {
        String name;
        Integer count;
        System.out.println("name: ");
        name = scanner.nextLine();
        System.out.println("count: ");
        count = Integer.valueOf(scanner.nextInt());
        System.out.println("price: ");
        controller.addToAnExistingItem(name, count);
    }


    private static void printOperation(String itemName) {
        System.out.println(controller.getItemFromWarehouse(itemName).toString());
    }

    private static void addNewItem(Scanner scanner) {
        String name;
        Integer count;
        Integer price;
        System.out.println("Add an item: ");
        System.out.println("name: ");
        name = scanner.nextLine();
        System.out.println("count: ");
        count = Integer.valueOf(scanner.nextInt());
        System.out.println("price: ");
        price = Integer.valueOf(scanner.nextInt());
        controller.addNewItem(name, new Item(name, count, price));
    }
}
