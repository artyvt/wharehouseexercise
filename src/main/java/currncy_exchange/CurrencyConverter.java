package currncy_exchange;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CurrencyConverter {

    public static void main(String[] args) throws IOException {
        String rawJson = getCurrencyRates();
        JsonObject jsObject = JsonParser.parseString(rawJson).getAsJsonObject();
        JsonObject rates = jsObject.get("rates").getAsJsonObject();

        List<RateData> rateData = new ArrayList<>();

        for (Map.Entry<String, JsonElement> rateEntry : rates.entrySet()) {
            Currency currency = Currency.valueOf(rateEntry.getKey());
            BigDecimal value = rateEntry.getValue().getAsBigDecimal();
            RateData rate = new RateData(currency, value);
            rateData.add(rate);
        }

        System.out.println("EUR currency converter, please enter exchange currency to convert to: ");
        Scanner scanner = new Scanner(System.in);
        Currency currency = Currency.valueOf(scanner.nextLine());

        System.out.println("Enter the amount: ");
        BigDecimal amount = new BigDecimal(scanner.nextLine());

        RateData correctRateDAta = null;
        for (RateData rate :
                rateData) {
            if (currency == rate.getCurrency()) {
                correctRateDAta = rate;
            }
        }

        BigDecimal result = amount.multiply(correctRateDAta.getRate());
        System.out.println("Result: " + result + " " + currency);

        System.out.println(getCurrencyRates());

    }

    private static String getCurrencyRates() throws IOException, MalformedURLException {
        URL url = new URL("https://api.exchangeratesapi.io/latest");
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
        BufferedReader br = new BufferedReader(inputStreamReader);
        String output = br.readLine();
        conn.disconnect();
        return output;
    }

}
