package work_with_files.simple_file;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class SimpleFileMain {
    public static void main(String[] args) {

        File file = new File("/home/artursvaitilavics/IdeaProjects/test/src/main/java/work_with_files/simple_file_001.txt");
        String info001 = "This is what I am writing to the file";

        createNewFile(file);
        writeToFile(info001, file);

        readFromFile(file);
    }

    private static void readFromFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
        } catch (Exception e) {
            System.out.println("Can't read the file: ");
            e.printStackTrace();
        }
    }

    private static void writeToFile(String string, File file) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(string);
            fileWriter.close();
            System.out.println("Successfully wrote to the file");
        } catch (Exception e) {
            System.out.println("Can't write to the file!");
            e.printStackTrace();
        }
    }

    private static void createNewFile(File file) {
        try {
            if (file.createNewFile()) {
                System.out.println(file.getName() + " created!");
            } else {
                System.out.println(file.getName() + " already exists!");
            }

        } catch (Exception e) {
            System.out.println("file was not created! ");
            e.printStackTrace();
        }
    }
}
