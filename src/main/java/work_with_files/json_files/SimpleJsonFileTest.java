package work_with_files.json_files;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class SimpleJsonFileTest {
    public static void main(String[] args) {
        Map<String, String> myMap = new HashMap<>();

        myMap.put("father", "Arturs");
        myMap.put("mother", "Diana");
        myMap.put("daughter", "Helena");

        System.out.println(myMap.toString());
        System.out.println();

        String relation = "grandmother";
        String name = "Dace";

        JsonObject jsonObject = new JsonObject();
        myMap.entrySet().stream().forEach(entry -> {
            jsonObject.addProperty(entry.getKey(), entry.getValue());
        });

        String filePath = "/home/artursvaitilavics/IdeaProjects/test/src/main/java/work_with_files/json_files/json_test.json";
        File file = new File(filePath);
        try {
            if (!file.exists()){
                file.createNewFile();
            }else{
                System.out.println("file already exists");
            }
        } catch (Exception e) {
            System.out.println("Did not create the file, error: ");
            e.printStackTrace();
        }
        //TODO: read from file , place stuff in json object, append stuff, and overwrite the file, create backup
        //TODO: from existing file
//        jsonObject.addProperty(relation, name);

        try {
            FileWriter fileWriter = new FileWriter(file);
//            fileWriter.write(jsonObject.toString());

            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            System.out.println("Did not write to the file, error: ");
            e.printStackTrace();
        }

    }
}
