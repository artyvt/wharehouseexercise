package exercises.exercise_02;

import java.util.ArrayList;
import java.util.List;

public class Items implements AbstractItems {
    private List<Item> itemList = new ArrayList<>();

    public Items(Item item) {
        itemList.add(item);
    }

    public String getName() {
        return itemList.get(0).getName() + "s";
    }

    public Integer getCount() {
        return itemList.size();
    }

    public Integer setPrice(Integer unitPrice) {
        return unitPrice * itemList.size();
    }

    @Override
    public Integer getPrice() {
        return itemList.size() * itemList.get(0).getPrice();
    }

    public void add(Item item) {
        itemList.add(item);
    }

    public void addMultiples(Item item, Integer count) {
        for (int i = 0; i < count; i++) {
            add(new Item(item.getName()));
        }
    }

    public void removeItem() {
        itemList.remove(itemList.size() - 1);
    }

    public void removeMultiples(Integer count) {
        Integer tempCount = 0;
        if (count > itemList.size()) {
            System.out.println("Not enough " + getName());
            return;
        }
        while (tempCount < count) {
            removeItem();
            tempCount++;
        }
    }

    public List getItemsList() {
        return itemList;
    }

}
