package exercises.exercise_02;


import java.util.HashMap;
import java.util.Map;

public class _WhareHouse {
    private static _WhareHouse instance;

    public _WhareHouse() {
    }

    public static _WhareHouse getInstance(){
        if (instance == null){
            instance = new _WhareHouse();
        }
        return instance;
    }

    private Map<String, Items> whareHouse = new HashMap<>();

    public void addItems(Items items) {
        whareHouse.put(items.getName(), items);
    }

    public Integer getCount() {
        return whareHouse.size();
    }


    @Override
    public String toString() {
        return whareHouse.toString();
    }
}
