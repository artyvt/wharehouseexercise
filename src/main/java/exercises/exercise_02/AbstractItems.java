package exercises.exercise_02;

public interface AbstractItems {

    String getName();

    Integer getCount();

    Integer getPrice();
}
