package exercises.exercise_02;


//TODO: will need  a controller for adding, removing from where house.
public class WareHouseMain {
    public static void main(String[] args) {
        Item hammerItem = new Item("hammer");
        hammerItem.setPrice(10);
        System.out.println(hammerItem.getName());
        System.out.println(hammerItem.getPrice());

        Items hammers = new Items(hammerItem);
        System.out.println(hammers.getCount());
        hammers.add(hammerItem);
        System.out.println(hammers.getCount());
        hammers.addMultiples(hammerItem, 2);
        System.out.println(hammers.getCount());
        System.out.println(hammers.getPrice());
        hammers.removeItem();
        System.out.println(hammers.getCount());
        System.out.println(hammers.getPrice());
        hammers.removeMultiples(15);
        System.out.println(hammers.getCount());
        System.out.println(hammers.getPrice());


        Item saw = new Item("saw");
        saw.setPrice(14);

        Items sawItems = new Items(saw);
        sawItems.addMultiples(saw, 3);
        System.out.println(sawItems.getName() + " " + sawItems.getCount());


        _WhareHouse whareHouse = _WhareHouse.getInstance();
        whareHouse.addItems(hammers);
        whareHouse.addItems(sawItems);
        System.out.println(whareHouse.toString());


    }

}
