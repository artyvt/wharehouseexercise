package dice_game.players;

public class Gladiator extends Player {
    public Gladiator(String name, Integer healthPoints) {
        super(name, healthPoints);
    }

    @Override
    public Integer rollDice() {
        Integer result;
        if (this.getHealthPoints() == 1) {
            result = specialAbility();
        } else {
            result = super.rollDice();
        }
        return result;
    }

    @Override
    public Integer specialAbility() {
        Integer result;
        System.out.println("Gladiator special ability!");
        Integer roll01 = super.rollDice();
        Integer roll02 = super.rollDice();
        if (roll01 > roll02) {
            result = roll01;
        } else {
            result = roll02;
        }
        return result;
    }
}
