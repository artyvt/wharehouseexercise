package dice_game.players;

public class Archer extends Player {
    private Integer takeHitCounter = 3;
    private Integer dontTakeDamage = 2;


    public Archer(String name, Integer healthPoints) {
        super(name, healthPoints);
    }

    @Override
    public void loseHP(Integer damageAmount) {
        takeHitCounter--;
        if (takeHitCounter == 0) {
            if (dontTakeDamage != 0) {
                damageAmount = specialAbility();
                damageAmount--;
            }
        }
        super.loseHP(damageAmount);
    }

    @Override
    public Integer specialAbility() {
        Integer result;
        if (this.getHealthPoints() == 3) {
            result = 0;
            System.out.println("Archer special ability");
        } else {
            result = 1;
        }
        return result;
    }
}
