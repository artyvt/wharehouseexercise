package dice_game.players;

import java.util.Random;

public abstract class Player {
    private String name;
    private Integer healthPoints;

    public Player(String name, Integer healthPoints) {
        this.name = name;
        this.healthPoints = healthPoints;
    }

    public Integer rollDice() {
        Random random = new Random();
        Integer rolledNumber = random.nextInt(6) + 1;
        System.out.println(this.getName() + " rolled: " + rolledNumber);//TODO: remove from here print statement
        return rolledNumber;
    }

    public String getName() {
        return name;
    }

    public Integer getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(Integer healthPoints) {
        this.healthPoints = healthPoints;
    }


    public void loseHP(Integer damageAmount) {
        this.healthPoints--;
    }

    public abstract Integer specialAbility();
}
