package dice_game.players;

public class Tiger extends Player {

    public Tiger(String name, Integer healthPoints) {
        super(name, healthPoints);
    }

    @Override
    public void loseHP(Integer damageAmount) {
        boolean specialAbilityOn = true;
        if (this.getHealthPoints() == 3 && specialAbilityOn) {
            damageAmount = specialAbility();
            specialAbilityOn = false;
        }
        super.loseHP(damageAmount);
    }

    @Override
    public Integer specialAbility() {
        Integer result;
        if (this.getHealthPoints() == 3) {
            result = 0;
            System.out.println("Tiger special ability");
        } else {
            result = 1;
        }
        return result;
    }
}
