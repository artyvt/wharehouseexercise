package dice_game;

import com.google.gson.JsonObject;
import dice_game.arena.BattleArena;
import dice_game.game_stats.ResultsObjects;
import dice_game.players.Archer;
import dice_game.players.Gladiator;
import dice_game.players.Tiger;

import java.io.File;
import java.io.FileWriter;

public class DiceGameMain {

    public static void main(String[] args) {
        BattleArena battleArena = new BattleArena();

        Gladiator gladiator001 = new Gladiator("Maximus", 2);
        Tiger tiger001 = new Tiger("Khan", 4);
        Archer archer001 = new Archer("Robin", 3);
        Tiger tiger002 = new Tiger("Khan", 4);


        System.out.println("\t\t\tTest Arena\n--------------------------------------------");


        battleArena.startBattle(tiger001, gladiator001);


//        System.out.println(battleArena.getResults().toString());

        ResultsObjects resultsObjects = new ResultsObjects();

        battleArena.getResults().entrySet().stream().forEach(integerStringEntry -> {
            resultsObjects.setRoundResults(integerStringEntry.getKey(), integerStringEntry.getValue());
        });

        System.out.println(resultsObjects.getRoundResults().toString());

        JsonObject jsonObject = new JsonObject();
        resultsObjects.getRoundResults().entrySet().stream().forEach(entry -> {
            jsonObject.addProperty(entry.getValue(), entry.getKey());
        });


        System.out.println("json looks like: ");
        System.out.println(jsonObject.toString());
        try {
            File file = new File("RoundResults.json");
            FileWriter fileWriter = new FileWriter(file);
            if (file.exists()) {
                System.out.println(file.getName() + " already exists");
            } else {
                file.createNewFile();
            }
            fileWriter.write(jsonObject.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            System.out.println("Could not create json file: ");
            e.printStackTrace();
        }

    }
}
