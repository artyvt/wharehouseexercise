package dice_game.arena;

import dice_game.game_stats.ResultsObjects;
import dice_game.players.Player;

import java.util.HashMap;
import java.util.Map;

public class BattleArena {

    private Integer rounds = 0;

    private Map<Integer, String> results = new HashMap<>();

    //TODO: make bellow lines work, super nice Json file
//    private Map<String, Integer> roundsMap = new HashMap<>();
//    private Map<String, Integer> rollingResults = new HashMap<>();


    public void startBattle(Player player01, Player player02) {

        while (player01.getHealthPoints() > 0 && player02.getHealthPoints() > 0) {
            rounds++;
            Integer firstPlayerNumber = player01.rollDice();
            Integer secondPlayerNumber = player02.rollDice();

            String winner = takeDamageFromLoser(player01, player02, firstPlayerNumber, secondPlayerNumber);


            results.put(rounds, winner);

        }
    }


    private String winnerOfTheGame(Player player01, Player player02) {
        String winner = null;
        if (player01.getHealthPoints().equals(0)) {
            winner = player02.getName();
        } else if (player02.getHealthPoints().equals(0)) {
            winner = player01.getName();
        }
        return winner;
    }

    private String takeDamageFromLoser(
            Player player01,
            Player player02,
            Integer firstPlayerNumber,
            Integer secondPlayerNumber) {
        String roundWinner = null;

        if (firstPlayerNumber > secondPlayerNumber) {
            player02.loseHP(1);
            roundWinner = player01.getName();

        } else if (firstPlayerNumber < secondPlayerNumber) {
            player01.loseHP(1);
            roundWinner = player02.getName();
        } else {
            roundWinner = "draw";
        }
        return roundWinner;
    }


    public Integer getRounds() {
        return rounds;
    }

    public Map<Integer, String> getResults() {
        return results;
    }

}
