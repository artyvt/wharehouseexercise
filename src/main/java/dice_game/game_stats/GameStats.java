package dice_game.game_stats;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class GameStats {
    private JsonObject games = new JsonObject();
    private JsonArray rounds = new JsonArray();



    public JsonObject getGames() {
        return games;
    }

    public void setGames(Integer roundNumber, JsonArray rounds) {

        this.games.add("game: " + roundNumber.toString(), rounds);

    }
}
