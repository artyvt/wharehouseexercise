package dice_game.game_stats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultsObjects {

    private Map<Integer, String> roundResults = new HashMap<>();

    public void setRoundResults(Integer round, String results) {
        roundResults.put(round, results);
    }

    public Map<Integer, String> getRoundResults() {
        return roundResults;
    }
}
